public class No {
	private int valor;
	private No esquerdo;
	private No direito;

	public No(int valor) {
		super();
		this.valor = valor;
	}

	public No(int valor, No esquerdo, No direito) {
		super();
		this.valor = valor;
		this.esquerdo = esquerdo;
		this.direito = direito;
	}

	public void setNull() {
		this.valor = 0;
		this.direito = null;
		this.esquerdo = null;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public No getEsquerdo() {
		return esquerdo;
	}

	public void setEsquerdo(No esquerdo) {
		this.esquerdo = esquerdo;
	}

	public No getDireito() {
		return direito;
	}

	public void setDireito(No direito) {
		this.direito = direito;
	}

}