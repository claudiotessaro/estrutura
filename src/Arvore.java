
/**
 * 
 * @author claudio tessaro
 *
 */
public class Arvore {
	/**
	 * 
	 * @param raiz
	 * @param valor
	 * @return
	 */
	public No inserir(No raiz, int valor) {
		if (!isEmpty(raiz)) {
			if (valor < raiz.getValor()) {
				if (raiz.getEsquerdo() != null) {
					inserir(raiz.getEsquerdo(), valor);
				} else {
					raiz.setEsquerdo(new No(valor));
				}

			} else if (valor > raiz.getValor()) {
				if (raiz.getDireito() != null) {
					inserir(raiz.getDireito(), valor);
				} else {
					raiz.setDireito(new No(valor));

				}
			}
		} else {
			raiz = new No(valor);
		}

		return raiz;
	}

	/**
	 * 
	 * @param no
	 */
	public void emOrdem(No no) {
		if (no != null) {
			emOrdem(no.getEsquerdo());
			System.out.println(no.getValor() + " ");
			emOrdem(no.getDireito());
		}
	}

	/**
	 * 
	 * @param no
	 */
	public void preOrdem(No no) {
		if (no != null) {
			preOrdem(no.getEsquerdo());
			preOrdem(no.getDireito());
		}
	}

	/**
	 * 
	 * @param no
	 */
	public void posOrdem(No no) {
		if (no != null) {
			posOrdem(no.getEsquerdo());
			posOrdem(no.getDireito());


		}
	}

	/**
	 * 
	 * @param no
	 * @param valor
	 * @return
	 */
	public No remover(No no, int valor) {
		if (isEmpty(no)) {
			throw new IllegalArgumentException("lista vazia");
		}
		if (valor < no.getValor()) {
			no.setEsquerdo(remover(no.getEsquerdo(), valor));
		} else if (valor > no.getValor()) {
			no.setDireito(remover(no.getDireito(), valor));
		} else if (no.getEsquerdo() != null && no.getDireito() != null) {
			no.setValor(encontrarMinimo(no.getDireito()).getValor());
			no.setDireito(null);
		} else {
			no = (no.getEsquerdo() == null) ? no.getEsquerdo() : no.getDireito();
		}
		return no;
	}

	/**
	 * 
	 * @param raiz
	 * @return
	 */
	private boolean isEmpty(No raiz) {
		return raiz == null;
	}

	/**
	 * 
	 * @param raiz
	 * @return
	 */
	public No encontrarMinimo(No raiz) {
		No atual = raiz;
		No anterior = null;
		while (atual != null) {
			anterior = atual;
			atual = atual.getEsquerdo();
		}
		return anterior;
	}

	/**
	 * 
	 * @param atual
	 * @return
	 */
	public int altura(No atual) {
		int valor = 0;
		if (atual == null || (atual.getEsquerdo() == null && atual.getEsquerdo() == null))
			valor = 0;
		else {
			if (altura(atual.getEsquerdo()) > altura(atual.getDireito())) {
				valor =  (1 + altura(atual.getEsquerdo()));
			} else {
				valor = (1 + altura(atual.getDireito()));
			}
		}
		return valor;
	}

	/**
	 * 
	 * @param raiz
	 * @return
	 */
	public int numeroDeFolhas(No raiz) {
		int quantidadeDeFolhas = 0;
		if (raiz != null) {
			numeroDeFolhas(raiz.getEsquerdo());
			numeroDeFolhas(raiz.getDireito());
		} else {
			quantidadeDeFolhas++;
		}
		return quantidadeDeFolhas;
	}

	/**
	 * 
	 * @param raiz
	 * @return
	 */
	public int numeroDeNos(No raiz) {
		int quantidadeDeFolhas = 0;
		if (raiz != null) {
			numeroDeNos(raiz.getEsquerdo());
			quantidadeDeFolhas++;
			numeroDeNos(raiz.getDireito());
		}
		return quantidadeDeFolhas;
	}

	/**
	 * 
	 * @param no
	 * @return
	 */
	public boolean isCheia(No no) {
		boolean estaCheio = true;
		if (no != null) {
			if ((no.getEsquerdo().getEsquerdo() != null) && (no.getEsquerdo().getDireito() == null)
					|| (no.getDireito().getEsquerdo() != null) && (no.getDireito().getDireito() == null)) {
				estaCheio = false;
			}
			numeroDeNos(no.getEsquerdo());
			numeroDeNos(no.getDireito());
		}
		return estaCheio;
	}

}
